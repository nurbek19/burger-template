import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import BurgerBuilder from "./containers/BurgerBuilder/BurgerBuilder";
import Checkout from './containers/Checkout/Checkout';
import Layout from './components/Layout/Layout';
import Orders from "./containers/Orders/Orders";


class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/checkout" component={Checkout}/>
                    <Route path="/" exact component={BurgerBuilder}/>
                    <Route path="/orders" component={Orders}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
