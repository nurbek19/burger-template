import React, {Component, Fragment} from 'react';
import Modal from '../../components/UI/Modal/Modal';
import axios from 'axios';

const withErrorHandler = (WrappedComponent) => {
    return class extends Component {
        constructor(props) {
            super(props);

            this.state = {
                error: null
            };

            axios.interceptors.response.use(response => response, error => {
                this.setState({error: error});
            })
        }

        errorDismissed = () => {
          this.setState({error: null});
        };

        render() {
            return (
                <Fragment>
                    <Modal show={this.state.error} closed={this.errorDismissed}>This is error message</Modal>
                    <WrappedComponent {...this.props}/>
                </Fragment>
            )
        }
    }
};

export default withErrorHandler;