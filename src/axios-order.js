import axios from 'axios';

const instance = axios.create({
   baseURL: 'https://burger-template-2faae.firebaseio.com'
});

// instance.interceptors.request.use((req) => {
//    console.log('[In request interceotor}', req);
//    return req;
// });
//
// instance.interceptors.response.use((res) => {
//    console.log('[In response interceptor}', res);
//    return res;
// }, (err) => console.log(err));

export default instance;