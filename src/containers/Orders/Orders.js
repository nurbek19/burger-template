import React, {Component} from 'react';
import OrderItem from '../../components/Order/OrderItem/OrderItem';
import withErrorHandler from '../../hoc/withErroHandler/withErrorHandler';
import axios from '../../axios-order';

class Orders extends Component {
    state = {
        orders: [],
        loading: true
    };

    componentDidMount() {
        axios.get('/orders.json').then(response => {
            const fetchedOrders = [];

            for(let key in response.data) {
                fetchedOrders.push({...response.data[key], id: key});
            }

            this.setState({loading: false, orders: fetchedOrders});
        }).catch(() => {
            this.setState({loading: false});
        })
    }

    render() {
        let orders = this.state.orders.map(order => {
            return <OrderItem
                key={order.id}
                ingredients={order.ingredients}
                price={order.price}
            />
        });

        if(this.state.loading) {
            orders = <p>Loading...</p>
        }

        return orders;

    }
}

export default withErrorHandler(Orders, axios);