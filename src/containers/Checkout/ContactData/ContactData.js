import React, {Component} from 'react';
import {connect} from 'react-redux';

import Button from '../../../components/UI/Buttons/Buttons';
import './ContactData.css';
import axios from '../../../axios-order';

class ContactData extends Component {
    state = {
        name: '',
        email: '',
        street: '',
        postal: '',
        loading: false
    };

    valueChanged = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };

    orderHandler = event => {
        event.preventDefault();

        this.setState({loading: true});

        const order = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            customer: {
                name: this.state.name,
                email: this.state.email,
                street: this.state.street,
                postal: this.state.postal
            }
        };

        axios.post('/orders.json', order).then(() => {
            this.setState({loading: false});
            this.props.history.push('/');
        });

    };

    render() {
        return (
            <div className="ContactData">
                <h4>Enter your Contact Data</h4>
                <form>
                    <input
                        className="Input"
                        type="text" name="name"
                        placeholder="Your Name"
                        value={this.state.name}
                        onChange={this.valueChanged}
                    />
                    <input className="Input" type="email" name="email" placeholder="Your Mail"
                           value={this.state.email}
                           onChange={this.valueChanged}
                    />
                    <input className="Input" type="text" name="street" placeholder="Street"
                           value={this.state.street}
                           onChange={this.valueChanged}
                    />
                    <input className="Input" type="text" name="postal" placeholder="Postal Code"
                           value={this.state.postal}
                           onChange={this.valueChanged}
                    />
                    <Button btnType="Success" clicked={this.orderHandler}>ORDER</Button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.bb.ingredients,
        price: state.bb.totalPrice
    }
};

export default connect(mapStateToProps)(ContactData);
