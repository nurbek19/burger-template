import * as actionTypes from './actionTypes';

export const orderRequest = () => {
  return {type: actionTypes.ORDER_REQUEST}
};

export const orderSuccess = () => {
  return {types: actionTypes.ORDER_SUCCESS}
};

export const orderError = () => {
    return {types: actionTypes.ORDER_ERROR}
};